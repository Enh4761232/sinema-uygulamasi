﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201010Win_SinemaUygulamasıBitbucket
{
    class Seans
    {
        public Seans()
        {
            Salonlar = new List<Salon>();//salonlar belirlenmiş oldu
        }
        public TimeSpan BaslamaSaati { get; set; }
        public TimeSpan BitisSaati { get; set; }
        public List<Salon> Salonlar { get; set; }//aynı seans saatinde aynı film birden fazla salonda oynayabilir


    }
}
