﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201010Win_SinemaUygulamasıBitbucket
{
    class Film
    {
        public Film()
        {
            //ctor tab tab -constructor
            Seanslar = new List<Seans>();//Film clasından nesne üretildiğinde otomatik olarak list tipinde seans gelir
        }
        //sınıf kendi içerisinde kendisinin listesini tutuyor.
        //bir özelliği static olarak ayarlar isek o özellik sınıfın içerisinde tanımlı kalır.
        //static elemanlar instance üretilmeden kullanılan alanlardır.
        //formda film f=new film(); diyerek özelliklere ulaşırız.fakat static eleman olduğunda Film.Filmler dememiz yeterli olur.
        //static elemanlar kavram olarak nesneye bağlı fakat instance almadan nesneye ulaşabilen elemanlardır.projede bir defa oluşturulur.sürekli yeniden yeniden kullanılmaz

        private static List<Film> filmler = new List<Film>();
        public static List<Film> Filmler
        {
            get { return filmler; }
            set { filmler = value; }
        }

        public string Adi { get; set; }
        public string Tur { get; set; }
        public TimeSpan Sure { get; set; }
        public string Aciklama { get; set; }
        public List<Seans> Seanslar { get; set; }



    }
}
