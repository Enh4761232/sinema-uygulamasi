﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201010Win_SinemaUygulamasıBitbucket
{
    class Salon
    {
        public Salon()
        {
            Koltuklar = new List<Koltuk>();
        }
        //bir özelliği static olarak ayarlar isek o özellik sınıfın içerisinde tanımlı kalır.

        public string Sube { get; set; }
        public string Adi { get; set; }
        public List<Koltuk> Koltuklar { get; set; }

    }
}
