﻿namespace _201010Win_SinemaUygulamasıBitbucket
{
    partial class FilmTanimForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtFilmAdi = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.maskedTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.txtFilmAciklama = new System.Windows.Forms.RichTextBox();
            this.nmrSure = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFilmTur = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbSeansSalon = new System.Windows.Forms.ComboBox();
            this.lstSeanslar = new System.Windows.Forms.ListBox();
            this.btnSeansEkle = new System.Windows.Forms.Button();
            this.txtSeansBitisSaati = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSeansBaslamaSaati = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnFilmOlustur = new System.Windows.Forms.Button();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmrSure)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Adı";
            // 
            // txtFilmAdi
            // 
            this.txtFilmAdi.Location = new System.Drawing.Point(123, 37);
            this.txtFilmAdi.Name = "txtFilmAdi";
            this.txtFilmAdi.Size = new System.Drawing.Size(143, 22);
            this.txtFilmAdi.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.maskedTextBox2);
            this.groupBox1.Controls.Add(this.txtFilmAciklama);
            this.groupBox1.Controls.Add(this.nmrSure);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtFilmTur);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtFilmAdi);
            this.groupBox1.Location = new System.Drawing.Point(53, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 280);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Film Özellikleri";
            // 
            // maskedTextBox2
            // 
            this.maskedTextBox2.Location = new System.Drawing.Point(819, 62);
            this.maskedTextBox2.Name = "maskedTextBox2";
            this.maskedTextBox2.Size = new System.Drawing.Size(100, 22);
            this.maskedTextBox2.TabIndex = 0;
            // 
            // txtFilmAciklama
            // 
            this.txtFilmAciklama.Location = new System.Drawing.Point(123, 129);
            this.txtFilmAciklama.Name = "txtFilmAciklama";
            this.txtFilmAciklama.Size = new System.Drawing.Size(143, 110);
            this.txtFilmAciklama.TabIndex = 3;
            this.txtFilmAciklama.Text = "";
            // 
            // nmrSure
            // 
            this.nmrSure.Location = new System.Drawing.Point(123, 96);
            this.nmrSure.Name = "nmrSure";
            this.nmrSure.Size = new System.Drawing.Size(143, 22);
            this.nmrSure.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Süre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Açıklama";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tür";
            // 
            // txtFilmTur
            // 
            this.txtFilmTur.Location = new System.Drawing.Point(123, 65);
            this.txtFilmTur.Name = "txtFilmTur";
            this.txtFilmTur.Size = new System.Drawing.Size(143, 22);
            this.txtFilmTur.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbSeansSalon);
            this.groupBox2.Controls.Add(this.lstSeanslar);
            this.groupBox2.Controls.Add(this.btnSeansEkle);
            this.groupBox2.Controls.Add(this.txtSeansBitisSaati);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtSeansBaslamaSaati);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(401, 48);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(292, 415);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Seans";
            // 
            // cmbSeansSalon
            // 
            this.cmbSeansSalon.FormattingEnabled = true;
            this.cmbSeansSalon.Location = new System.Drawing.Point(137, 110);
            this.cmbSeansSalon.Name = "cmbSeansSalon";
            this.cmbSeansSalon.Size = new System.Drawing.Size(121, 24);
            this.cmbSeansSalon.TabIndex = 3;
            // 
            // lstSeanslar
            // 
            this.lstSeanslar.FormattingEnabled = true;
            this.lstSeanslar.ItemHeight = 16;
            this.lstSeanslar.Location = new System.Drawing.Point(22, 205);
            this.lstSeanslar.Name = "lstSeanslar";
            this.lstSeanslar.Size = new System.Drawing.Size(252, 180);
            this.lstSeanslar.TabIndex = 2;
            // 
            // btnSeansEkle
            // 
            this.btnSeansEkle.Location = new System.Drawing.Point(139, 150);
            this.btnSeansEkle.Name = "btnSeansEkle";
            this.btnSeansEkle.Size = new System.Drawing.Size(119, 27);
            this.btnSeansEkle.TabIndex = 1;
            this.btnSeansEkle.Text = "Seans Ekle";
            this.btnSeansEkle.UseVisualStyleBackColor = true;
            // 
            // txtSeansBitisSaati
            // 
            this.txtSeansBitisSaati.Location = new System.Drawing.Point(137, 71);
            this.txtSeansBitisSaati.Mask = "00:00";
            this.txtSeansBitisSaati.Name = "txtSeansBitisSaati";
            this.txtSeansBitisSaati.Size = new System.Drawing.Size(121, 22);
            this.txtSeansBitisSaati.TabIndex = 0;
            this.txtSeansBitisSaati.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Salon";
            // 
            // txtSeansBaslamaSaati
            // 
            this.txtSeansBaslamaSaati.Location = new System.Drawing.Point(137, 37);
            this.txtSeansBaslamaSaati.Mask = "00:00";
            this.txtSeansBaslamaSaati.Name = "txtSeansBaslamaSaati";
            this.txtSeansBaslamaSaati.Size = new System.Drawing.Size(121, 22);
            this.txtSeansBaslamaSaati.TabIndex = 0;
            this.txtSeansBaslamaSaati.ValidatingType = typeof(System.DateTime);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Bitiş Saati";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Başlama Saati";
            // 
            // btnFilmOlustur
            // 
            this.btnFilmOlustur.Location = new System.Drawing.Point(176, 334);
            this.btnFilmOlustur.Name = "btnFilmOlustur";
            this.btnFilmOlustur.Size = new System.Drawing.Size(192, 57);
            this.btnFilmOlustur.TabIndex = 4;
            this.btnFilmOlustur.Text = "Oluştur";
            this.btnFilmOlustur.UseVisualStyleBackColor = true;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // FilmTanimForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 530);
            this.Controls.Add(this.btnFilmOlustur);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FilmTanimForm";
            this.Text = "FilmTanimForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmrSure)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFilmAdi;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nmrSure;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFilmTur;
        private System.Windows.Forms.MaskedTextBox maskedTextBox2;
        private System.Windows.Forms.RichTextBox txtFilmAciklama;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbSeansSalon;
        private System.Windows.Forms.ListBox lstSeanslar;
        private System.Windows.Forms.Button btnSeansEkle;
        private System.Windows.Forms.MaskedTextBox txtSeansBitisSaati;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox txtSeansBaslamaSaati;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnFilmOlustur;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
    }
}