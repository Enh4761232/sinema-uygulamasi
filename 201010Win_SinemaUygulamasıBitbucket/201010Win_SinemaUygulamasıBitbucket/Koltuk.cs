﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;//button için kullanılan kütüphane

namespace _201010Win_SinemaUygulamasıBitbucket
{
    class Koltuk:Button//koltuk buttondan inheritance aldı.miras aldı
    {
        public Koltuk()//default constructor
        {
            KoltukTanimla();//parametre almadan çalışan 
        }
        public Koltuk(int boyut)
        {
            KoltukTanimla(boyut);//parametre alarak çalışan constructor
        }
       void KoltukTanimla(int size=50)//size adlı parametre alabilirsin.opsiyonel yapıldı.varsayınal 30 değer verilirse verilen değer olur
        {
            Boyut = size;
            Width = size;
            Height = size;
            FlatStyle = FlatStyle.Flat;//bu da bir enum flat butonu gri nesne haline getiriyor.bizde oa resim koyacağız.
            Margin = new Padding(0);//margini 0 olarak ayarlandı
            BackgroundImage = new Bitmap(Properties.Resources.koltuk_Yesil,size,size);//varsayılan resim yaptık.resmin boyutunu 50 50 olarak şekilde ayarladık.bitmap dediğimiz şey resim

        }
        public int Boyut { get; set; }//koltuğun boyutu lazım olacak özellik olarak
        public int Numara { get; set; }
        public string Sira { get; set; }//abc sırası

        public Bilet Bilet { get; set; }
        //propfull tab tab ile field ve property li  tanımlanıyor.
        private KoltukDurum koltukdurum;//field dışarıdan erişilmeyecek
        //koltuk durumu propfull yapma sebebimiz koltuk seçildiğinde rengi değişecek o yüzden ana field değişmeden property üzerinden erişmek istiyoruz.o yüzdxen auto property tanımlanmıyor.bu durumuda propertynin set anında  kontrol edeceğiz
        public KoltukDurum KoltukDurum
        {

            get { return koltukdurum; }//dışardan erişebilecek olan
            set 
            {
                switch (value)
                {
                    case KoltukDurum.Bos:
                        BackgroundImage = new Bitmap(Properties.Resources.koltuk_Yesil, Boyut, Boyut);
                        break;
                    case KoltukDurum.Satildi:
                        BackgroundImage = new Bitmap(Properties.Resources.koltuk_Kirmizi, Boyut, Boyut);
                        break;
                    case KoltukDurum.Rezerve:
                        BackgroundImage = new Bitmap(Properties.Resources.koltuk_Mavi, Boyut, Boyut);
                        break;
                    case KoltukDurum.Kapali:
                        BackgroundImage = new Bitmap(Properties.Resources.koltuk_Gri, Boyut, Boyut);
                        break;
                    default:
                        break;
                    //koltuk nesnesi eklendiğinde properties kısmından koltuk durum değerleri değiştiği zaman duruma göre  koltuk rengi değişiyor

                }
                koltukdurum = value; 
            }

        }

        //public KoltukDurum Durum { get; set; }
        // properties altında resimleri resources.resx dosyasından çekebilmek gibi bir avantajımız var

    }
    enum KoltukDurum //KoltukDurum tipinde property tanımladık içerisine sadece listede olanlardan değer atayabiliriz çünkü enum var.Enum seçenek listesi program içerisinde sadece listedekilerden seçilebilir
    {
        Bos,
        Satildi,
        Rezerve,
        Kapali
    }
}